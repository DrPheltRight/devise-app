describe User do
  subject { described_class.new(email: 'example@example.com') }
  its(:email) { is_expected.to eq('example@example.com') }
end
