Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'devise/registrations',
                                    confirmations: 'auth/confirmations' }

  devise_scope :user do
    get  '/login',            to: 'devise/sessions#new',         as: :login
    get  '/logout',           to: 'devise/sessions#destroy',     as: :logout
    get  '/register',         to: 'devise/registrations#new',    as: :register
    get  '/password/recover', to: 'devise/passwords#new',        as: :recover_password
    post '/password/recover', to: 'devise/passwords#create',     as: :reset_password
    get  '/password/change',  to: 'devise/passwords#edit',       as: :edit_password
    put  '/password/change',  to: 'devise/passwords#update',     as: :update_password

    patch '/confirm' => 'auth/confirmations#confirm', as: :confirm
  end

  root 'welcome#index'
end
