class FormBuilder < ActionView::Helpers::FormBuilder
  def errors(attribute)
    errors = object.errors.messages[attribute] || []

    unless errors.empty?
      "<span class=\"field__error\">#{attribute.to_s.humanize} #{errors.join(', ')}</span>".html_safe
    end
  end
end
